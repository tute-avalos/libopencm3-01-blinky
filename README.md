# Tutorial de programación de ARM Cortex-M con herramientas libres

Para este tutorial estoy utilizando:

* **HARDWARE:** Blue Pill (STM32F103C8) y ST-Link/V2
* **SOFTWARE:** PlatformIO en VS Code con libOpenCM3 sobre Debian GNU/Linux.

## Ejemplo 01: Blinky

Este ejemplo está apuntado a ver los aspectos básicos del Software a utilizar, para más información:

Entrada del blog: [Intro a la programación de ARM Cortex-M con Software Libre](https://electronlinux.wordpress.com/2020/05/29/intro-a-la-programacion-de-arm-cortex-m-con-software-libre/)

Video en YouTube: [Programación de ARM Cortex-M con Software Libre](https://www.youtube.com/watch?v=LpQGSGRlzPI)

